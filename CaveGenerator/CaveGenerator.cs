using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;

namespace CaveConverter
{
    //
    // This is a console application that converts DATA statements 
    // in the original Creative Computing Wumpus program listing 
    //  
    //   http://www.atariarchives.org/bcc1/showpage.php?page=250 
    //
    // such as
    //
    //   DATA 2,5,8,1,3,10,2,4,12,3,5,14,1,4,6
    //
    // to a friendlier, modern XML file representation of same.
    //
    // David Bautista
    // davidb@vertigosoftware.com
    //
    public class CaveGenerator
    {
        
        private static readonly string AppName = Process.GetCurrentProcess().ProcessName;

        private const int UsageError = 1;
        private const int InputFileError = 2;
        private const int OutputFileError = 3;

        private static readonly string UsageString =
            string.Format("{0}:\n" +
            "Usage:  {0} <source file> <output file>\n" +
            "<source file>:  The file containing data statements.\n" +
            "<output file>:  The file to recieve the output xml.\n",
            AppName);

        [STAThread]
        public static int Main(string[] argv) 
        {
                       
            if (argv.Length != 2) 
            {
                Console.Write(UsageString);
                return UsageError;
            }

            var linkQueue = new Queue();
            int i;

            try
            {
                using (var streamReader = File.OpenText(argv[0]))
                {
                    var buffer = new StringBuilder();

                    while (streamReader.Peek() >= 0)
                    {
                        var readyForLinks = false;
                        buffer.Length = 0;
                        var line = streamReader.ReadLine() ?? string.Empty;
                        for (i = 0; i < line.Length; i++)
                        {
                            if (readyForLinks)
                            {
                                switch (line[i])
                                {
                                    case ',':
                                        linkQueue.Enqueue(buffer.ToString().TrimStart());
                                        buffer.Length = 0;
                                        continue;
                                    default:
                                        buffer.Append(line[i]);
                                        break;
                                }
                                continue;
                            }
                            if (char.IsWhiteSpace(line[i]))
                            {
                                if (buffer.ToString().ToUpper() == "DATA")
                                    readyForLinks = true;
                                buffer.Length = 0;
                                continue;
                            }
                            buffer.Append(line[i]);
                        }
                    }
                }
            } 
            catch 
            {
                return InputFileError;
            }
            
            
            if (linkQueue.Count == 0
                || linkQueue.Count % 3 != 0) 
            {
                return InputFileError;
            }

            try 
            {
                using (var xtw = new XmlTextWriter(argv[1], Encoding.UTF8))
                {
                    xtw.WriteStartDocument();
                    xtw.WriteStartElement("Wumpus");
                    xtw.WriteStartElement("Nodes");

                    const int nodeCount = 1;
                    while (linkQueue.Count > 0)
                    {
                        xtw.WriteStartElement("Node");
                        xtw.WriteStartAttribute("Name", string.Empty);
                        xtw.WriteString(nodeCount.ToString(CultureInfo.InvariantCulture));
                        xtw.WriteEndAttribute();
                        xtw.WriteStartElement("Links");
                        for (i = 0; i < 3; i++)
                        {
                            xtw.WriteStartElement("Link");
                            xtw.WriteStartAttribute("Name", string.Empty);
                            xtw.WriteString((string)linkQueue.Dequeue());
                            xtw.WriteEndElement();
                        }
                        xtw.WriteEndElement();

                        xtw.WriteEndElement();
                    }

                    xtw.WriteEndElement();
                    xtw.WriteEndElement();
                    xtw.WriteEndDocument();
                }
            } 
            catch 
            {
                return OutputFileError;
            }
                       
            return 0;
        }
        
    }
}