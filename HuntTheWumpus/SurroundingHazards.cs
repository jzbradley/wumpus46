using System;

namespace HuntTheWumpus
{
    [Flags]
    public enum SurroundingHazards
    {
        BatNearby = 1,
        None = 0,
        PitNearby = 2,
        WumpusNearby = 4
    }
}