#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

#endregion

namespace HuntTheWumpus
{
    //
    // ported to C# from the original 1972 BASIC program listing:
    //  
    //   http://www.atariarchives.org/bcc1/showpage.php?page=250 
    //
    // David Bautista
    // davidb@vertigosoftware.com
    //
    public class TextPresentation
    {

        private const string Instructions =
            @"
WELCOME TO WUMPUS II
THIS VERSION HAS THE SAME RULES AS 'HUNT THE WUMPUS'.
HOWEVER, YOU NOW HAVE A CHOICE OF CAVES TO PLAY IN.
SOME CAVES ARE EASIER THAN OTHERS. ALL [default] CAVES HAVE 20
ROOMS AND 3 TUNNELS LEADING FROM ONE ROOM TO OTHER ROOMS.
THE CAVES ARE:
    0  -  DODECAHEDRON   THE ROOMS OF THIS CAVE ARE ON A
        12-SIDED OBJECT, EACH FORMING A PENTAGON.
        THE ROOMS ARE AT THE CORNERS OF THE PENTAGONS.
        EACH ROOM HAVING TUNNELS THAT LEAD TO 3 OTHER ROOMS.
            
    1  -  MOBIUS STRIP   THIS CAVE IS TWO ROOMS
        WIDE AND 10 ROOMS AROUND (LIKE A BELT)
        YOU WILL NOTICE THERE IS A HALF TWIST
        SOMEWHERE.
            
    2  -  STRING OF BEADS    FIVE BEADS IN A CIRCLE.
        EACH BEAD IS A DIAMOND WITH A VERTICAL
        CROSS-BAR. THE RIGHT & LEFT CORNERS LEAD
        TO NEIGHBORING BEADS. (THIS ONE IS DIFFICULT
        TO PLAY.
            
    3  -  HEX NETWORK        IMAGINE A HEX TILE FLORE.
        TAKE A RECTANGLE WITH 20 POINTS (INTERSECTIONS)
        INSIDE (4X4). JOIN RIGHT & LEFT SIDES TO MAKE A
        CYLINDER. THEN JOIN TOP & BOTTOM TO FORM A 
        TORUS (DOUGHNUT).
        HAVE FUN IMAGINING THIS ONE!!
            
    CAVES 1-3 ARE REGULAR IN A SENSE THAT EACH ROOM
GOES TO THREE OTHER ROOMS & TUNNELS ALLOW TWO-
WAY TRAFFIC. HERE ARE SOME 'IRREGULAR' CAVES:
            
    4  -  DENDRITE WITH DEGENERACIES   PULL A PLANT FROM
        THE GROUND. THE ROOTS & BRANCHES FORM A 
        DENDRITE. IE., THERE ARE NO LOOPING PATHS
        DEGENERACIES MEANS A) SOME ROOMS CONNECT TO
        THEMSELVES AND B) SOME ROOMS HAVE MORE THAN ONE
        TUNNEL TO THE SAME ROOM IE, 12 HAS 
        TWO TUNNELS TO 13.
            
    5  -  ONE WAY LATTICE     HERE ALL TUNNELS GO ONE
        WAY ONLY. TO RETURN, YOU MUST GO AROUND THE CAVE
        (AROUND 5 MOVES).
            
    6  -  ENTER YOUR OWN CAVE    The computer will ask you 
        for a filename.  It should point to an XML file with 
        a schema, identical to that, used by the included 
        Wumpus XML files.  The nodes can have non-numeric 
        names and zero or more linked nodes.  The only 
        requirement is that you include sufficient nodes for 
        the player, wumpus, pits, and bats.

    7  -  RANDOM CAVE    The computer will ask you for a
        seed, then generate a random cave with 20 rooms;
        2 bats; 2 pits; and, of course, a wumpus! Also,
        potential deadends. Which you may start in. Good
        luck and as always -

    HAPPY HUNTING!
";


        [STAThread]
        public static int Main(string[] argv)
        {
            IOAdapter.Write(
                "*** Wumpus .NET ***\n\nBased on:\n\n\n{0}\n{1}\n{2}\n\n\n\n",
                "WUMPUS 2".PadLeft(33), "CREATIVE COMPUTING".PadLeft(38),
                "MORRISTOWN  NEW JERSEY".PadLeft(40));

            IOAdapter.Write("INSTRUCTIONS");
            var input = IOAdapter.ReadLine()?.ToUpper() ?? string.Empty;
            if (input.StartsWith("Y"))
            {
                IOAdapter.Write(Instructions);
            }
            var game = new Game();
            return game.Play();
        }
    }
}