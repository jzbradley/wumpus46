namespace HuntTheWumpus
{
    public enum GameStatus
    {
        InPlay = 0,
        Lost,
        Won
    }
}