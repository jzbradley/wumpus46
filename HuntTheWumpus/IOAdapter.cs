﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace HuntTheWumpus
{
    public static class IOAdapter
    {
        public static Action<string> WriteMethod = Console.Write;
        public static Func<string> ReadMethod = Console.ReadLine;

        [StringFormatMethod("format")]
        public static void Write(string format, params object[] args)
        {
            WriteMethod?.Invoke(string.Format(format, args));
        }

        [StringFormatMethod("format")]
        public static void WriteLine(string format, params object[] args)
        {
            WriteMethod?.Invoke(string.Format(format, args));
            WriteMethod?.Invoke(Environment.NewLine);
        }

        public static string ReadLine()
        {
            return ReadMethod?.Invoke();
        }
    }
}
