#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.XPath;

#endregion

namespace HuntTheWumpus
{
    //
    // ported to C# from the original 1972 BASIC program listing:
    //  
    //   http://www.atariarchives.org/bcc1/showpage.php?page=250 
    //
    // David Bautista
    // davidb@vertigosoftware.com
    //
    public class WumpusLogic
    {
        public static readonly int DefaultBats = 2;
        public static readonly int DefaultPits = 2;
        public static readonly int MaxShots = 5;
        public static readonly int StartArrows = 5;

        private static Random _Random = new Random();

        private readonly Dictionary<string, Node> _initialBatNodes;
        private readonly Dictionary<string, Node> _initialPitNodes;
        private readonly Dictionary<string, Node> _nodes;

        private Dictionary<string, Node> _batNodes;
        private Dictionary<string, Node> _pitNodes;

        private Node _initialPlayerNode;
        private Node _initialWumpusNode;
        private Node _wumpusNode;
        private int _numBats = 2;
        private int _numPits = 2;
        private int _numRooms = 20;

        public WumpusLogic(string setupXmlString)
        {
            _initialBatNodes = new Dictionary<string, Node>();
            _initialPitNodes = new Dictionary<string, Node>();
            _nodes = new Dictionary<string, Node>();

            using (var stringReader = new StringReader(setupXmlString))
                SetupNodes(stringReader);
            PlaceItems();
            RestartGame();
        }

        public WumpusLogic(TextReader setupXmlReader)
        {
            _initialBatNodes = new Dictionary<string, Node>();
            _initialPitNodes = new Dictionary<string, Node>();
            _nodes = new Dictionary<string, Node>();

            SetupNodes(setupXmlReader);
            PlaceItems();

            RestartGame();
        }

        public WumpusLogic(int? seed = null)
        {
            _numBats = 2;
            _numPits = 2;
            _numRooms = 20;
            _Random = seed != null ? new Random(seed.Value) : new Random();
            _nodes = Enumerable
                .Range(1, _numRooms)
                .ToDictionary(i => i.ToString(), i => new Node(i.ToString()));
            _initialBatNodes = new Dictionary<string, Node>();
            _initialPitNodes = new Dictionary<string, Node>();
            Node unfilledNode;
            while ((unfilledNode = _nodes.Values.FirstOrDefault(n => n.LinkedNodes.Count < 3)) != null)
            {
                var link = _nodes[(_Random.Next(_numRooms) + 1).ToString()];
                unfilledNode.AddLinkedNode(link);
            }

            PlaceItems();

            RestartGame();
        }

        public int ArrowsRemaining { get; private set; }

        public IReadOnlyCollection<Node> BatNodes => Status == GameStatus.InPlay ? null : _batNodes.Values;

        public GameStatus Status { get; private set; }

        public IReadOnlyCollection<Node> Nodes => _nodes.Values;

        public Node PlayerNode { get; private set; }

        public IReadOnlyCollection<Node> PitNodes => Status == GameStatus.InPlay ? null : _pitNodes.Values;

        public Node WumpusNode => Status == GameStatus.InPlay ? null : _wumpusNode;

        public Node GetNodeByName(string nodeName)
        {
            return _nodes[nodeName];
        }

        public SurroundingHazards GetSurroundingHazards()
        {
            var results = PlayerNode == _wumpusNode
                ? SurroundingHazards.WumpusNearby
                : SurroundingHazards.None;

            foreach (var node in PlayerNode.LinkedNodes.Where(node => node != null))
            {
                if (node == _wumpusNode)
                    results |= SurroundingHazards.WumpusNearby;
                if (_pitNodes.ContainsKey(node.Name))
                    results |= SurroundingHazards.PitNearby;
                if (_batNodes.ContainsKey(node.Name))
                    results |= SurroundingHazards.BatNearby;
            }

            return results;
        }

        public IEnumerable<MovementResult> Move(string nodeName)
        {
            return Move(nodeName, false);
        }

        private IEnumerable<MovementResult> Move(string nodeName, bool allowNonAdjacentMove)
        {
            var results = new Queue<MovementResult>();
            var node = _nodes[nodeName];
            if (node == null
                || !allowNonAdjacentMove && !PlayerNode.IsAdjacentNode(node)
                || Status != GameStatus.InPlay)
            {
                results.Enqueue(MovementResult.IllegalMove);
                return results;
            }

            var result = MovePlayer(node);
            results.Enqueue(result);
            switch (result)
            {
                case MovementResult.BumpedAWumpus:
                    MoveWumpus();
                    results.Enqueue(MovementResult.GotByWumpus);
                    Status = GameStatus.Lost;
                    break;
                case MovementResult.FellInAPit:
                    Status = GameStatus.Lost;
                    break;
                case MovementResult.SuperBatSnatch:
                    nodeName = _nodes.Keys.ElementAt(_Random.Next(_nodes.Count));
                    if (nodeName == null) break;
                    foreach (var movementResult in Move(nodeName, true))
                        results.Enqueue(movementResult);
                    break;
            }

            return results;
        }

        private MovementResult MovePlayer(Node node)
        {
            PlayerNode = node;

            if (_pitNodes.ContainsKey(node.Name)) return MovementResult.FellInAPit;
            if (_batNodes.ContainsKey(node.Name)) return MovementResult.SuperBatSnatch;
            if (PlayerNode == _wumpusNode) return MovementResult.BumpedAWumpus;
            return MovementResult.SuccessfulMove;
        }

        private void MoveWumpus()
        {
            _wumpusNode = _wumpusNode
                .LinkedNodes
                .Where(n => n != null)
                .ElementAt(_Random.Next(_wumpusNode.LinkedNodes.Count));
        }

        private void PlaceItems()
        {
            int i;
            Node node;

            if (_nodes.Count <= (_numBats + _numPits + 2))
                throw new Exception("Insufficient nodes for game.");

            var keys = _nodes.Keys.ToList();

            // Place player.
            var key = keys[_Random.Next(keys.Count)];
            _initialPlayerNode = _nodes[key];
            keys.Remove(key);

            // Place wumpus.
            key = keys[_Random.Next(keys.Count)];
            _initialWumpusNode = _nodes[key];
            keys.Remove(key);

            // Place pits.
            for (i = 0; i < _numPits; i++)
            {
                key = keys[_Random.Next(keys.Count)];
                node = _nodes[key];
                _initialPitNodes[node.Name] = node;
                keys.Remove(key);
            }

            // Place bats.    
            for (i = 0; i < _numPits; i++)
            {
                key = keys[_Random.Next(keys.Count)];
                node = _nodes[key];
                _initialBatNodes[node.Name] = node;
                keys.Remove(key);
            }
        }

        public void RestartGame()
        {
            ArrowsRemaining = StartArrows;
            _batNodes = new Dictionary<string, Node>(_initialBatNodes);
            Status = GameStatus.InPlay;
            _pitNodes = new Dictionary<string, Node>(_initialPitNodes);
            PlayerNode = _initialPlayerNode;
            _wumpusNode = _initialWumpusNode;
        }

        private void SetupNodes(TextReader setupXmlReader)
        {
            var document = new XPathDocument(setupXmlReader);
            var navigator = document.CreateNavigator();
            _numBats = Convert.ToInt32(navigator.Evaluate("sum(/Wumpus/Bats/@number)"));
            if (_numBats < 1) _numBats = DefaultBats;
            _numPits = Convert.ToInt32(navigator.Evaluate("sum(/Wumpus/Pits/@number)"));
            if (_numPits < 1) _numPits = DefaultPits;

            var linkNameExpression = navigator.Compile("Links/Link/@name");
            _numRooms = 0;
            foreach (XPathNavigator current in navigator.Select("/Wumpus/Nodes/Node"))
            {
                _numRooms++;
                var nodeName = current.GetAttribute("name", string.Empty);
                if (nodeName == string.Empty) continue;
                Node node;
                if (!_nodes.TryGetValue(nodeName, out node))
                {
                    node = new Node(nodeName);
                    _nodes.Add(nodeName, node);
                }
                var linkNodes =
                    current.Select(linkNameExpression)
                        .Cast<XPathNavigator>()
                        .Select(link => link.Value)
                        .Where(linkNodeName => linkNodeName != string.Empty);
                foreach (var linkNodeName in linkNodes)
                {
                    Node linkNode;
                    if (!_nodes.TryGetValue(linkNodeName, out linkNode))
                    {
                        linkNode = new Node(linkNodeName);
                        _nodes.Add(linkNodeName, linkNode);
                    }
                    node.AddLinkedNode(linkNode);
                }
            }
        }

        public ShotResult Shoot(IReadOnlyCollection<Node> targetNodes)
        {
            if ((ArrowsRemaining < 1) || (Status != GameStatus.InPlay)) return ShotResult.NoMoreArrows;

            ArrowsRemaining -= targetNodes.Count;
            var tempNode = PlayerNode;
            var enumerator = targetNodes.GetEnumerator();
            
            for (var i = 0; (i < MaxShots) && enumerator.MoveNext(); i++)
            {
                var node = enumerator.Current;

                if (tempNode.IsAdjacentNode(node))
                {
                    tempNode = node;
                }
                else
                {
                    node = tempNode.LinkedNodes.ElementAt(_Random.Next(tempNode.LinkedNodes.Count));
                    i = MaxShots; // break after the following logic
                }

                if (node == _wumpusNode)
                {
                    Status = GameStatus.Won;
                    return ShotResult.ShotWumpus;
                }

                if (node == PlayerNode)
                {
                    Status = GameStatus.Lost;
                    return ShotResult.ShotSelf;
                }
            }

            if (ArrowsRemaining < 1)
            {
                Status = GameStatus.Lost;
                return ShotResult.NoMoreArrows;
            }

            MoveWumpus();
            if (_wumpusNode != PlayerNode) return ShotResult.Missed;

            Status = GameStatus.Lost;
            return ShotResult.GotByWumpus;
        }
    }
}