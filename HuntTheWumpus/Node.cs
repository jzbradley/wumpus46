using System.Collections.Generic;

namespace HuntTheWumpus
{
    public class Node
    {
        private readonly Dictionary<string, Node> _links;

        public Node(string name)
        {
            _links = new Dictionary<string, Node>();
            Name = name;
        }

        public IReadOnlyCollection<Node> LinkedNodes => _links.Values;
        public string Name { get; }

        public void AddLinkedNode(Node node)
        {
            _links.Add(node.Name, node);
        }

        public bool IsAdjacentNode(Node node)
        {
            return node != null && IsAdjacentNode(node.Name);
        }

        public bool IsAdjacentNode(string nodeName)
        {
            return _links.ContainsKey(nodeName);
        }

        public void ClearNodeLinks()
        {
            _links.Clear();
        }
    }
}