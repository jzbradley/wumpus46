namespace HuntTheWumpus
{
    public enum MovementResult
    {
        IllegalMove = 0,
        BumpedAWumpus,
        FellInAPit,
        GotByWumpus,
        SuccessfulMove,
        SuperBatSnatch
    }
}