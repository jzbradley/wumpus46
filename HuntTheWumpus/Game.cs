﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntTheWumpus
{
    public class Game
    {
        private const string BeadsFilename = "Caves\\StringOfBeads.xml";
        private const string DendriteFilename = "Caves\\DendriteWithDegeneracies.xml";
        private const string DodecahedronFilename = "Caves\\Dodecahedron.xml";
        private const string LatticeFilename = "Caves\\OneWayLattice.xml";
        private const string MobiusFilename = "Caves\\MobiusStrip.xml";
        private const string TorusFilename = "Caves\\HexNetOnTorus.xml";
        private const int InputFileError = 1;
        private const int GameError = 2;

        private const string GotByWumpusString =
            "TSK TSK TSK- WUMPUS GOT YOU!\n";

        public int Play()
        {
            var result = 0;
            var playAgain = false;
            WumpusLogic wumpusLogic = null;
            do
            {
                if (wumpusLogic != null)
                    continue;

                var setupFileName = string.Empty;
                string input;
                do
                {
                    IOAdapter.Write("CAVE #(0-6) ");
                    input = IOAdapter.ReadLine();
                    IOAdapter.Write("\n");
                    input = input?.Trim() ?? string.Empty;
                    if (input == string.Empty)
                    {
                        input = "X";
                    }
                    switch (input[0])
                    {
                        case '0':
                            setupFileName = DodecahedronFilename;
                            break;
                        case '1':
                            setupFileName = MobiusFilename;
                            break;
                        case '2':
                            setupFileName = BeadsFilename;
                            break;
                        case '3':
                            setupFileName = TorusFilename;
                            break;
                        case '4':
                            setupFileName = DendriteFilename;
                            break;
                        case '5':
                            setupFileName = LatticeFilename;
                            break;
                        case '6':
                            IOAdapter.Write("Wumpus XML Filename: ");
                            setupFileName = IOAdapter.ReadLine()?.Trim() ?? string.Empty;
                            break;
                        case '7':
                            setupFileName = null;
                            break;
                        default:
                            IOAdapter.Write("ERROR  \n");
                            IOAdapter.ReadLine();
                            IOAdapter.Write("\n");
                            break;
                    }
                } while (setupFileName == string.Empty);

                if (setupFileName != null)
                {
                    setupFileName = Path.Combine(Environment.CurrentDirectory, setupFileName);

                    StreamReader streamReader = null;
                    try
                    {
                        streamReader = File.OpenText(setupFileName);
                        wumpusLogic = new WumpusLogic(streamReader);
                    }
                    catch
                    {
                        IOAdapter.Write(
                            "{0}:\nError: Unable to read Wumpus XML file, {1}.\n",
                            AppName, setupFileName);
                        result = InputFileError;
                    }
                    finally
                    {
                        streamReader?.Close();
                    }
                }
                else
                {
                    IOAdapter.Write("SEED?\n");
                    var seed = IOAdapter
                        .ReadLine()
                        ?.Select(c => (int)c).Aggregate((left, right) =>
                        {
                            unchecked
                            {
                                return left * 397 +
                                       right;
                            }
                        });
                    wumpusLogic = new WumpusLogic(seed);
                }

                if (result != 0
                    || wumpusLogic == null)
                    continue;
                IOAdapter.Write("HUNT THE WUMPUS\n");
                try
                {
                    while (wumpusLogic.Status == GameStatus.InPlay)
                    {
                        PrintHazardWarningsAndLocation(wumpusLogic);
                        IOAdapter.Write("SHOOT OR MOVE ");
                        input = (IOAdapter.ReadLine() ?? string.Empty).TrimStart().ToUpper();
                        switch (input[0])
                        {
                            case 'M':
                                HandleMove(wumpusLogic);
                                continue;
                            case 'S':
                                HandleShoot(wumpusLogic);
                                continue;
                        }
                        IOAdapter.Write("ERROR   ");
                        IOAdapter.ReadLine();
                    }
                    IOAdapter.Write(
                        wumpusLogic.Status == GameStatus.Lost
                            ? "HA HA HA - YOU LOOSE!\n"
                            : "HEE HEE HEE - THE WUMPUS'LL GET YOU NEXT TIME!!\n");
                    IOAdapter.Write("PLAY AGAIN");
                    input = (IOAdapter.ReadLine() ?? string.Empty).TrimStart().ToUpper();
                    playAgain = input.StartsWith("Y");
                    IOAdapter.Write("\n\n");
                    if (playAgain)
                    {
                        IOAdapter.Write("SAME SET-UP ");
                        input = (IOAdapter.ReadLine() ?? string.Empty).TrimStart().ToUpper();
                        if (input.StartsWith("Y"))
                        {
                            wumpusLogic.RestartGame();
                        }
                        else
                        {
                            wumpusLogic = null;
                        }
                    }
                }
                catch
                {
                    IOAdapter.Write(
                        "{0}:\nError: Game error.\n");
                    result = GameError;
                }
            } while (playAgain && result == 0);

            return result;
        }

        private static readonly string AppName = Process.GetCurrentProcess().ProcessName;

        private static void HandleMove(WumpusLogic wumpusLogic)
        {
            string input;
            do
            {
                IOAdapter.Write("WHERE TO ");
                input = IOAdapter.ReadLine()?.Trim() ?? string.Empty;
                IOAdapter.Write("\n");
                if (wumpusLogic.PlayerNode.IsAdjacentNode(input)) continue;
                IOAdapter.Write("NOT POSSIBLE - {0}\n", input);
                IOAdapter.ReadLine();
                input = string.Empty;
            } while (input == string.Empty);

            foreach (var movementResult in wumpusLogic.Move(input))
            {
                switch (movementResult)
                {
                    case MovementResult.BumpedAWumpus:
                        IOAdapter.Write("... OOPS! BUMPED A WUMPUS!\n");
                        break;
                    case MovementResult.FellInAPit:
                        IOAdapter.Write("YYYIIIEEEE . . . FELL IN A PIT\n");
                        break;
                    case MovementResult.GotByWumpus:
                        IOAdapter.Write(GotByWumpusString);
                        break;
                    case MovementResult.SuperBatSnatch:
                        IOAdapter.Write(
                            "ZAP--SUPER BAT SNATCH! ELSEWHERESVILLE FOR YOU!\n");
                        break;
                }
            }
        }

        public static void HandleShoot(WumpusLogic wumpusLogic)
        {
            string input;
            int numShots;

            do
            {
                IOAdapter.Write("NO. OF ROOMS ");
                input = IOAdapter.ReadLine()?.Trim() ?? string.Empty;
                try
                {
                    numShots = Convert.ToInt32(input);
                }
                catch
                {
                    numShots = -1;
                }

                if ((numShots >= 1) && (numShots <= WumpusLogic.MaxShots)) continue;
                numShots = -1;
                IOAdapter.Write("ERROR   ");
                IOAdapter.ReadLine();
            } while (numShots < 1);

            var shots = new Queue<Node>();
            while (numShots > 0)
            {
                IOAdapter.Write("ROOM #");
                input = IOAdapter.ReadLine()?.Trim() ?? string.Empty;
                IOAdapter.Write("\n");
                var node = wumpusLogic.GetNodeByName(input);

                if (node == null)
                {
                    IOAdapter.Write("ERROR   ");
                    IOAdapter.ReadLine();
                }
                else
                {
                    shots.Enqueue(node);
                    numShots--;
                }
            }

            switch (wumpusLogic.Shoot(shots))
            {
                case ShotResult.GotByWumpus:
                    IOAdapter.Write(GotByWumpusString);
                    break;
                case ShotResult.Missed:
                    IOAdapter.Write("MISSED\n");
                    break;
                case ShotResult.NoMoreArrows:
                    IOAdapter.Write("YOU HAVE USED ALL YOUR ARROWS.\n");
                    break;
                case ShotResult.ShotSelf:
                    IOAdapter.Write("OUCH! ARROW GOT YOU!\n");
                    break;
                case ShotResult.ShotWumpus:
                    IOAdapter.Write(
                        "AHA! YOU GOT THE WUMPUS! HE WAS IN ROOM {0}\n",
                        wumpusLogic.WumpusNode.Name);
                    break;
            }
        }

        private static void PrintHazardWarningsAndLocation(WumpusLogic wumpusLogic)
        {
            IOAdapter.Write("\n");

            var hazards = wumpusLogic.GetSurroundingHazards();
            if ((hazards & SurroundingHazards.WumpusNearby) != 0)
            {
                IOAdapter.Write("I SMELL A WUMPUS!\n");
            }
            if ((hazards & SurroundingHazards.PitNearby) != 0)
            {
                IOAdapter.Write("I FEEL A DRAFT!\n");
            }
            if ((hazards & SurroundingHazards.BatNearby) != 0)
            {
                IOAdapter.Write("BATS NEARBY!\n");
            }

            IOAdapter.Write("YOU ARE IN ROOM {0}\n", wumpusLogic.PlayerNode.Name);

            IOAdapter.Write("TUNNELS LEAD TO");
            var enumerator = wumpusLogic.PlayerNode.LinkedNodes.GetEnumerator();
            while (enumerator.MoveNext())
            {
                var node = enumerator.Current;
                if (node == null) continue;
                IOAdapter.Write(" {0}", node.Name);
            }
            IOAdapter.Write("\n\n");
        }
    }
}
